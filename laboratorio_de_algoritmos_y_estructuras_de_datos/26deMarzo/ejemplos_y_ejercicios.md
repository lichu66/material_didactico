----
#### Laboratorio de Algoritmos y Estructuras de Datos
----

```c++

  // Ejemplo  if - else - if
 
  
  cout << "Ingrese un número." << endl;

  int numeroIngresado;

  if (numeroIngresado % 2 == 0){
     cout << "El número ingresado es múltiplo de 2." << endl;
  } else if (numeroIngresado % 3 == 0){
     cout << "El número ingresado es múltiplo de 3." << endl;
  } else {
     cout << "El número ingresado no es múltiplo de 2 ni múltiplo de 3" << endl;
  }
  

  // Ejemplo de switch case

  cout << "Ingrese un número." << endl;

  int numero;

  cin >> numero;

  switch(numero) {
    case 1:
      cout << "Ingresó el número 1" << endl;
      break;

    case 2:
      cout << "Ingresó el número 2" << endl;
      break;

    case 3:
      cout << "Ingresó el número 3" << endl;
      break;

    case 4:
      cout << "Ingresó el número 4" << endl;
      break;

    case 5:
      cout << "Ingresó el número 5" << endl;
      break;

    default:
      cout << "Ingresó un número negativo o mayor a 5" << endl;
  }


  // Ejemplo para imprimir mensaje combinado con variables

  cout << "Ingrese una palabra." << endl;

  string palabra;
  
  cin >> palabra;
  
  cout << "La palabra ingresada fue: " << palabra << "." << endl;



```


#### Ejercicios

- Crear el pseudocódigo, el diagrama de flujo y el programa en c++.

1. Crear un programa que le pida al usuario un nombre y luego imprima
un saludo donde se visualice el nombre recibido.

2. Crear un programa que le pida al usuario un número entero y luego imprima
la triplicación del número recibido.

3. Crear un programa que le pida los siguientes datos al usuario: nombre,
edad, dni, curso. El programa deberá imprimir la información recibida
con el siguiente formato:
```
  - nombre:
  - edad:
  - dni:
  - curso:

```

4. Crear un programa que sirva para calcular el volumen de un cilindro.
El programa deberá pedir el radio y la altura del mismo. El cálculo
a realizar es el siguiente: 3.14 * radio * radio * altura.

5. Crear un programa que reciba un número y luego imprima un mensaje
indicando si el mismo es múltiplo de 3 o no lo es.

6. Crear un programa que reciba un número y luego indique si el número
es múltiplo de 2 y múltiplo de 4, simultáneamente. 

7. Crear un programa que reciba un número y luego indique si el mismo
pertenece al intervalo ```[100, +∞)```, es decir, si el número es mayor o
igual a 100.

8. Crear un programa que reciba un número y luego indique si el mismo 
pertenece al siguiente intervalo: ```[10,900] U [1000, 4000)```.

9. Crear un programa que reciba un número y luego indique si el mismo
pertenece al siguiente intervalo: ``` (-∞,900) U [2000,6000] ```.

10. Crear un programa que reciba un número y luego indique si 

